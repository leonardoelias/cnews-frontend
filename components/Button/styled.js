import styled from 'styled-components';

const ButtonElement = styled.button `
  background: #4a90e2;
  color: #ffffff;
  position: relative;
  cursor: pointer;
  border-width: 1px;
  border-style: solid;
  line-height: 1;
  font-weight: normal;
  margin: 0;
  outline: none;
  box-sizing: border-box;
  vertical-align: middle;
  text-align: center;
  text-decoration: none;
  font-size: 1rem;
  border-radius: 4px;
  padding: calc(0.533rem - 1px) 1.066rem;
  height: 2.066rem;

  &[disabled] {
    color: #93a8ce;
    border-color: #eaeff8;
    background-color: #f7f9fe;
    cursor: not-allowed;
    pointer-events: none;
  }
`;

export default ButtonElement;
