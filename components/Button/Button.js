/**
 * @flow
 */
import * as React from 'react';
import ButtonElement from "./styled";

type Nature = 'default' | 'primary' | 'success' | 'info' | 'alarm' | 'warning';

type Size = 'xlarge' | 'large' | 'medium' | 'small' | 'xsmall';

type Props = {
  children?: React.ReactNode,
  onClick?: () => void,
  nature?: Nature,
  link?: boolean,
  size?: Size,
  theme?: any,
  circle?: boolean,
  selected?: boolean
};

class Button extends React.Component < Props > {
  static defaultProps = {
    children: '',
    nature: 'default',
    size: 'medium',
    link: false,
    selected: false,
    onClick: () => {}
  };

  render() {
    const {
      children,
      onClick,
      ...props
    } = this.props;
    return (
      <ButtonElement onClick={onClick} {...props}>
        {children}
      </ButtonElement>
    );
  }
}

export default Button;
