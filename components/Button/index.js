export {default as Button}
from './Button';

/**
 * @render react
 * @name Button
 * @description Buttons
 * @example
 * <Button nature="default">Default</Button>
 * <Button nature="primary"Default</Button>
 * <Button link>Default</Button>
 */
