const primaryList = ['#4a90e2', '#d0021b', '#bd10e0', '#7cbb37', '#f5a623'];

const blackList = [
  '#F8F8F8',
  '#F0F0F0',
  '#e3e3e3',
  '#cccccc',
  '#868686',
  '#888888',
  '#555555',
  '#333333',
  '#1e1e1e'
];

module.exports = {
  // b & w
  black: blackList[8],
  grayDark: blackList[6],
  gray: blackList[4],
  grayLight: blackList[2],
  white: '#ffffff',

  // ui colors
  blue: primaryList[0],
  red: '#d0021b',
  pink: '#bd10e0',
  green: '#7cbb37',
  orange: '#f5a623',
  primary: primaryList[0],
  info: '#00C3FF',
  success: '#2BCD86',
  alarm: '#EB5000',
  warning: '#FFB400',
  stable: blackList[4]
};
