// @flow

import * as React from 'react'
import Link from 'next/link'
import Head from 'next/head'

import {Button} from '../components/Button'

type Props = {
  children?: React.Node,
  title?: string
}

export default({
  children,
  title = 'This is the default title'
} : Props) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet='utf-8'/>
      <meta name='viewport' content='initial-scale=1.0, width=device-width'/>
    </Head>
    <header>
      <nav>
        <Link href='/'>
          <Button>Home</Button>
        </Link>
        <Link href='/category'>
          <Button>Category</Button>
        </Link>
        <Link href='/login'>
          <Button>Login</Button>
        </Link>
      </nav>
    </header>
    {children}
    <footer>
      footer
    </footer>
  </div>
)
