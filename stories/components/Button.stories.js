import React from 'react';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {linkTo} from '@storybook/addon-links';
import {withInfo} from '@storybook/addon-info';
import {injectGlobal} from 'styled-components';
import {withKnobs, text, boolean, number, select} from '@storybook/addon-knobs/react';
import {withNotes} from '@storybook/addon-notes';
import {Welcome} from '@storybook/react/demo';

import {Button} from "../../components";

injectGlobal `
  #root {
    padding: 20px;
  }
`;

storiesOf('Components | Button', module)
  .addDecorator(withKnobs)
  .add('Default', withInfo('Buttons default style')(withNotes('This is note')(() => (
    <Button>
      Default
    </Button>
  )),),);
