import React from 'react';
import styled from 'styled-components';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {linkTo} from '@storybook/addon-links';
import {withInfo} from '@storybook/addon-info';
import {withKnobs, text, boolean, number, select} from '@storybook/addon-knobs/react';
import {withNotes} from '@storybook/addon-notes';

storiesOf('Getting Started', module).add('Overview', withInfo(`info`)(() => (
  <Wrapper>
    <Button>Normal Button</Button>
    <Button primary>Primary Button</Button>
  </Wrapper>
)));

const Wrapper = styled.div `
  margin-right: 10px;
  margin-top: 10px;
  display: inline-block;
`;
const Button = styled.button `
  border-radius: 3px;
  padding: 0.25em 1em;
  margin: 0 1em;
  background: transparent;
  color: palevioletred;
  border: 2px solid palevioletred;
`;
