import {configure, addDecorator} from '@storybook/react';
import React from 'react';

import {ThemeProvider} from 'styled-components'
import Theme from '../components/Base/styles/colors';

import {setDefaults} from '@storybook/addon-info';
import {setOptions} from '@storybook/addon-options';

import '../components/Base/styles/global.css'

const req = require.context('../stories', true, /.stories.js$/);
function loadStories() {
  req
    .keys()
    .forEach(filename => req(filename));
}

setDefaults({
  styles: stylesheet => ({
    ...stylesheet,
    button: {
      ...stylesheet.button,
      topRight: {
        ...stylesheet.button.topRight,
        top: 'unset',
        bottom: '0'
      }
    }
  })
});

setOptions({hierarchySeparator: /\//, hierarchyRootSeparator: /\|/});

setOptions({name: 'CNews UI', url: '#', addonPanelInRight: false, sortStoriesByKind: true, sidebarAnimations: false});

addDecorator((story) => (
  <ThemeProvider theme={Theme}>
    {story()}
  </ThemeProvider>
))

configure(loadStories, module);
