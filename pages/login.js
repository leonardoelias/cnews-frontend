// @flow

import * as React from 'react'
import styled from 'styled-components'
import Layout from '../components/layout'

const Title = styled.h1 `
  color: red;
  font-size: 50px;
`

export default() => (
  <Layout title='Login'>
    <Title>Login</Title>
  </Layout>
)
